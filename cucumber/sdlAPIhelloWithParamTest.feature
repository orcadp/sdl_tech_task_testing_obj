Feature: Hello API check with param

  Background:
    Given Read Test API endpoint URL

  Scenario Outline: API check - GET hello/{par}
    And Extend endpoint with "<serviceMethod>"
    And Compose query by adding "<parameter>"
    And Request API with "<httpMethod>"
    Then Check that response status is "<status>"
    And Check fields in response "<expected_values>"

  Examples:
      | httpMethod | serviceMethod | parameter | status | expected_values |
      | GET | hello/ | \\  | 400 |  |
      | GET | hello/ | testname | 200 | Hello, testname |
      | GET | hello/ || 404 | |