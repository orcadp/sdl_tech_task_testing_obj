Feature: add API check with param

  Background:
    Given Read Test API endpoint URL

  Scenario Outline: API check - POST add/{par}
    And Extend endpoint with "<serviceMethod>"
    And Compose request body from enumeration "<parameter>"
    And Request API with "<httpMethod>"
    Then Check that response status is "<status>"
    And Check fields in response "<expected_values>"

  Examples:
      | httpMethod | serviceMethod | parameter | status | expected_values |
      | POST | add | 3,4,5  | 200 | 12 |
      | POST | add | foo, bar | 400 | |
      | POST | add || 500 | |