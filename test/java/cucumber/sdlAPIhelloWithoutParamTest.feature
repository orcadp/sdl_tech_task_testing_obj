Feature: Hello API check without params

  Background:
    Given Read Test API endpoint URL

  Scenario Outline: API check - GET hello
    And Extend endpoint with "<serviceMethod>"
    And Request API with "<httpMethod>"
    Then Check that response status is "<status>"
    And Check fields in response "<expected_values>"

  Examples:
      | httpMethod | serviceMethod | status | expected_values |
      | GET | hello |  200 | Hello |