package cucumber.steps;

import org.slf4j.Logger;

import java.net.HttpURLConnection;
import java.util.Properties;

abstract class sdlAPTestStepdefsBase {
    Properties stepsProps = new Properties();
    String endpointURL;
    HttpURLConnection httpConnection;
    Logger log;
}