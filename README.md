## Test Services
- test service: http://localhost:12345

- content service: http://localhost:9595/content/{name}


### To run the services
Run the following command in the project root directory
```
docker-compose up
 ```

### Swagger
Open http://petstore.swagger.io/ and replace the swagger.json
with http://localhost:12345/api-docs/swagger.json

### Description
Methods supported:
 
POST /add

GET /hello/{name}
where response is customised based on content service response

available names: id1, id2