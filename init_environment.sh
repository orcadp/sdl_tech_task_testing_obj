#!/bin/bash

#docker rm -f swaggerapi_microservice_1
#docker rm -f swaggerapi_content-mock_1

docker-compose -f docker-compose.yml down
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d
